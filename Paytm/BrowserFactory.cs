﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace Paytm
{
    
    public class BrowserFactory
    {
        public static IWebDriver driver;

       
        public static IWebDriver IntiBrowser(String browser_name,string url)
        {
            string init_browser = browser_name.ToLower();

            if (init_browser.Equals("chrome"))
            { 
            
                driver = new ChromeDriver();
            
            }
            else if(init_browser.Equals("firefox"))
            {

                driver = new FirefoxDriver();

            }

            driver.Url = url;

            driver.Manage().Window.Maximize();

            return driver;
        }
    }
}
