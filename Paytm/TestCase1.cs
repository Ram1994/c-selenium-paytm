﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Paytm
{
    [TestClass]
    public class TestCase1 : WebElementActions
    {
        public IWebDriver driver;

        [TestMethod]
        public void Click_Bus()
        {
            
            IWebDriver select_browser = BrowserFactory.IntiBrowser("chrome", "https://paytm.com/");

            driver = select_browser;

            var HomePage_pageobject = new HomePage_pageobjects(driver);

             PageFactory.InitElements(select_browser,HomePage_pageobject);

             HomePage_pageobject.Homepage_actions();



             var Bus_Booking_pageobject = new Bus_Booking_pageobjects(driver);

             PageFactory.InitElements(select_browser,Bus_Booking_pageobject);

             Bus_Booking_pageobject.Bus_Booking_actions();



             var Bus_Selection_pageobject = new Bus_Selection_pageobjects(driver);

             PageFactory.InitElements(select_browser, Bus_Selection_pageobject);

             Bus_Selection_pageobject.Choosing_Bus_actions();






        }



    }
}
