﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace Paytm
{
  
    public class HomePage_pageobjects : WebElementActions
    {

        public HomePage_pageobjects(IWebDriver driver)
        {
            this.driver = driver;
            
        }


        [FindsBy(How = How.XPath, Using = "(//*[@class='_1tnO'])[3]")]

        public IWebElement Bus_Button;


        public void Homepage_actions()
        {

            Button_Click(Bus_Button);
        }
    }
}
