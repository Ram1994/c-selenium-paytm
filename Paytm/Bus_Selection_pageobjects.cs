﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace Paytm
{
    
    public class Bus_Selection_pageobjects :WebElementActions
    {

        public Bus_Selection_pageobjects(IWebDriver driver)
        {
            this.driver = driver;
           
        }


        [FindsBy(How = How.XPath , Using = "(//div[@class='col-xs-9']//div[1]//div[@class='QqQk']//div[@class='col-xs-2'])[140]//button")]
        public IWebElement select_bus;

        [FindsBy(How = How.XPath, Using = "//div[@class='row searchBusTabs _14-1']//div//ul//li[1]")]
       public IWebElement scroll_to_this_element;

         [FindsBy(How = How.XPath , Using = "((//div[@class='row between-xs'])[3]//div)[64]//div")]
        public IWebElement upper_seat;

         [FindsBy(How = How.XPath, Using = "(//div[@class='_3wGa']//div//div//div//span)[1]")]
         public IWebElement boarding_point;

         [FindsBy(How = How.XPath, Using = "//*[@class='col-xs-5 LV9T']//ul//li[2]")]
         public IWebElement boarding_point_place;

         [FindsBy(How = How.XPath, Using = "(//div[@class='_3wGa']//div//div//div//span)[2]")]
         public IWebElement dropping_point;

         [FindsBy(How = How.XPath, Using = "//*[@class='col-xs-5 LV9T']//ul//li[2]")]
         public IWebElement dropping_point_place;

         [FindsBy(How = How.XPath, Using = "(//div[@class='col-xs-5 LV9T']//div)[22]//button")]
         public IWebElement continue_button;

        //[FindsBy(How = How.XPath , Using = "")]


         public void Choosing_Bus_actions()
         {


             selenium_waits();

             Button_Click(select_bus);

             IJavascriptexecutor(scroll_to_this_element);

             Button_Click(upper_seat);

             Button_Click(boarding_point);

             Button_Click(boarding_point_place);

             Button_Click(dropping_point);

             Actions_Element(dropping_point_place);

             Button_Click(continue_button);
                         
         
         }


    }
}
