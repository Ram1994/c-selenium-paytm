﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace Paytm
{
   
    public class Bus_Booking_pageobjects : WebElementActions
    {
        
        public Bus_Booking_pageobjects(IWebDriver driver)
        {

            this.driver = driver;
            

        }


        [FindsBy(How = How.Id, Using = "text-box")]
        public IWebElement source_text_box;

        [FindsBy(How = How.XPath, Using = "//div[@class='_2Alu']//div[1]//div[4]//div")]
        public IWebElement source_place;


        [FindsBy(How = How.XPath, Using = "(//*[@id='text-box'])[2]")]
        public IWebElement destination_text_box;


        [FindsBy(How = How.XPath, Using = "//div[@class='_2Alu']//div[1]//div[2]//div")]
        public IWebElement destination_place;


        [FindsBy(How = How.XPath , Using = "//*[@class='_2k43']")]
        public IWebElement date_picker;

        [FindsBy(How = How.XPath, Using = "//div[@class='react-datepicker']//div[@class='react-datepicker__month']//div")]
        public IList<IWebElement> select_date;

         [FindsBy(How = How.XPath , Using = "//div[1]//button[@class='button button--default button--bold cEYA']")]
        public IWebElement search_for_buses;

         //[FindsBy(How = How.XPath , Using = "")]
         //public IWebElement


        // [FindsBy(How = How.XPath , Using = "")]
        //public IWebElement



         public static void datepicker(IList<IWebElement> select_the_date)
         {

        

            // IList<IWebElement> month1 = driver1.FindElements(By.XPath("//div[@class='react-datepicker']//div[@class='react-datepicker__month']//div"));



             foreach (IWebElement dates in select_the_date)
             {
                 string selected_date = dates.Text;

                 if (selected_date.Equals("31"))
                 {

                     dates.Click();
                 }




             }
            
         
         
         }

        public void Bus_Booking_actions()
         {
             
            
            selenium_waits();
             
             Button_Click(source_text_box);

            Actions_Element(source_place);

             Button_Click(destination_text_box);

   //          selenium_waits(destination_text_box);

             Actions_Element( destination_place);

             Button_Click(date_picker);

             Bus_Booking_pageobjects.datepicker(select_date);

             Button_Click(search_for_buses);


         }


    }
}
