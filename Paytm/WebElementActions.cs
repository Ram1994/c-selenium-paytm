﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace Paytm
{
   
    public class WebElementActions
    {
        protected IWebDriver driver;
        public void Exception_block(Action method)
        {
            try
            {
                method();
            }
            catch
            {

                throw;
            }


        }


      

        public  void Button_Click(IWebElement element)
        {
            Exception_block(() => { element.Click(); });
            //element.Click();
        }

        public void Actions_Element(IWebElement element)
        {
            Exception_block(() =>
            {
                Actions acc = new Actions(driver);
                acc.Click(element).Build().Perform();
            });
            //Actions acc = new Actions(driver);
            //acc.Click(element).Build().Perform();

        }

        public void IJavascriptexecutor(IWebElement element)
        {
            Exception_block(() => { ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element); });
            //((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
        }

        public void selenium_waits()
        {

            Exception_block(() => { driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30); });
        
        }


    }
}
